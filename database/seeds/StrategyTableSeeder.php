<?php

use App\Strategy;
use Illuminate\Database\Seeder;

class StrategyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Strategy::create([
            'name' => 'random'
        ]);
        Strategy::create([
            'name' => 'weakest'
        ]);
        Strategy::create([
            'name' => 'strongest'
        ]);
    }
}
