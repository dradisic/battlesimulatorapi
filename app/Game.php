<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use SoftDeletes;

    public const STATUS_READY = 'ready';
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_FINISHED = 'finished';

    protected $guarded = ['id'];
    protected $appends = [
        'opponent',
        'overall_remaining_units',
    ];

    protected $fillable = [

    ];

    /**
     * The player who won the game.
     */
    public function winner(): belongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The player One.
     */
    public function playerOne(): belongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The player Two.
     */
    public function playerTwo(): belongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The logs that belongs to game.
     */
    public function logs(): hasMany
    {
        return $this->hasMany(Log::class);
    }

    /**
     * @return mixed
     */
    public function getOverallRemainingUnitsAttribute()
    {
        return $this->playerOne->armies->sum('remaining_units') + $this->playerTwo->armies->sum('remaining_units');
    }

    /**
     * @return User|null
     */
    public function getOpponentAttribute(): ?User
    {
        if ($this->playerOne()->exists() && $this->playerOne->id !== auth()->user()->id) {
            return $this->playerOne;
        }
        if ($this->playerTwo()->exists() && $this->playerTwo->id  !== auth()->user()->id) {
            return $this->playerTwo;
        }
    }

    /**
     * @return Boolean
     */
    public function getAlreadyJoinedAttribute()
    {
        if ($this->playerOne()->exists()) {
            $ids[] = $this->playerOne->id;
        }
        if ($this->playerTwo()->exists()) {
            $ids[] = $this->playerTwo->id;
        }

        return in_array(auth()->user()->id, $ids, true);
    }
}
