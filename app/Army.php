<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Army extends Model
{
    use SoftDeletes;

    /** @var float attack damage per unit */
    protected const ATTACK_DAMAGE = 0.5;

    /** @var int attack change per army */
    protected const ATTACK_CHANCE = 1;

    /** @var float Reload time in seconds per unit in the army */
    protected const RELOAD_TIME = 0.01;

    protected $fillable = [
        'name',
        'starting_units',
        'remaining_units',
        'strategy_id',
        'user_id'
    ];

    protected $appends = ['attack_chance', 'reload_time', 'attack_damage', 'is_defeated'];

    /**
     * @return BelongsTo
     */
    public function strategy(): BelongsTo
    {
        return $this->belongsTo(Strategy::class);
    }

    /**
     * @return float|int
     */
    public function getReloadTimeAttribute()
    {
        return self::RELOAD_TIME * $this->remaining_units;
    }

    /**
     * @return float|int
     * @throws Exception
     */
    public function getAttackChanceAttribute()
    {
        return $this->attackPossibility() ? 1 : 0;
    }

    /**
     * @return false|float
     */
    public function getAttackDamageAttribute()
    {
        return $this->remaining_units === 1 ? $this->remaining_units : floor(self::ATTACK_DAMAGE * $this->remaining_units);
    }

    /**
     * @return bool
     */
    public function getIsDefeatedAttribute(): bool
    {
        return $this->remaining_units < 1;
    }

    /**
     * @return mixed
     */
    public function getTarget($game)
    {
        switch ($this->strategy->name) {
            default:
            case 'random':
                $defender = $game->opponent->random_army;
                break;
            case 'weakest':
                $defender = $game->opponent->weakest_army;
                break;
            case 'strongest':
                $defender = $game->opponent->strongest_army;
                break;
        }

        return $defender;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    private function attackPossibility()
    {
        return random_int(0, 99) < self::ATTACK_CHANCE;
    }

    /**
     * @param $receivedDamage
     */
    public function receiveDamage($receivedDamage): void
    {
        $unitsLeft = $this->remaining_units - $receivedDamage;

        $this->remaining_units = $unitsLeft <= 0 ? 0 : $unitsLeft;
    }

    /**
     * Reset army units to starting value
     */
    public function reset(): void
    {
        $this->remaining_units = $this->starting_units;
        $this->save();
    }
}
