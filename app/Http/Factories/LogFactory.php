<?php


namespace App\Http\Factories;


use App\Log;

class LogFactory
{
    /**
     * @param $data
     * @return mixed
     */
    public static function create($data)
    {
        return Log::insert($data);
    }
}