<?php


namespace App\Http\Factories;


use App\Game;

class GameFactory
{
    /**
     * @return mixed
     */
    public static function create()
    {
        return Game::create([
            'status' => Game::STATUS_READY,
            'player_one_id' => auth()->user()->id
        ]);
    }

    /**
     * @param Game $game
     * @param $data
     */
    public static function update(Game $game, $data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param Game $game
     */
    public static function destroy(Game $game)
    {
        // TODO: Implement destroy() method.
    }
}