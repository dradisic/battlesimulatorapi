<?php


namespace App\Http\Factories;


use App\Army;

class ArmyFactory
{

    /**
     * @param $data
     * @return mixed
     */
    public static function create($data)
    {
        return Army::create([
            'name' => $data['name'],
            'starting_units' => $data['starting_units'],
            'remaining_units' => $data['starting_units'],
            'strategy_id' => $data['strategy'],
            'user_id' => auth()->user()->id,
        ]);
    }

    /**
     * @param Army $army
     * @param $data
     * @return bool
     */
    public static function update(Army $army, $data): bool
    {
        return $army->update([
            'name' => $data['name'],
            'starting_units' => $data['starting_units'],
            'remaining_units' => $data['remaining_units'],
            'strategy_id' => $data['strategy'],
        ]);
    }

    /**
     * @param Army $army
     * @return bool
     * @throws \Exception
     */
    public static function destroy(Army $army): bool
    {
        return $army->delete();
    }
}