<?php


namespace App\Http\Services;

use App\Army;
use App\Game;
use App\Http\Factories\LogFactory;
use Illuminate\Contracts\Auth\Authenticatable;
use RuntimeException;

class GameHandler
{
    /**
     * @param $game
     * @return bool
     */
    public static function startFight(Game $game): bool
    {
        $logData = [];

        foreach (auth()->user()->armies as $attacker) {
            if ($attacker->is_defeated) continue;

               $defender = $attacker->getTarget($game);
                if ($defender->is_defeated) continue;
                $battleLogData = self::war($attacker, $defender);
                $logData[] = array_merge($battleLogData, ['game_id' => $game->id]);
        }

        return LogFactory::create($logData);
    }

    /**
     * @param Authenticatable $user
     * @param Game $game
     * @return Game
     */
    public static function setUser(Authenticatable $user, Game $game): Game
    {
        if (!$game->playerOne()->exists()) {
            $game->player_one_id = $user->id;
            $game->save();
            return $game;
        }

        if (!$game->playerTwo()->exists()) {
            $game->player_two_id = $user->id;
            $game->save();
            return $game;
        }

        throw new RuntimeException('The maximum number of players is 2. You are not able to join this game.');
    }

    /**
     * @param $attacker
     * @param $defender
     * @return array
     */
    private static function war(Army $attacker, Army $defender): array
    {
        $damage = (int)floor($attacker->attack_chance * $attacker->attack_damage);
        $defender->receiveDamage($damage);

        $defender->save();

        $logData['attacker'] = $attacker->name;
        $logData['target'] = $defender->name;
        $logData['strategy'] = $attacker->strategy->name;
        $logData['damage'] = $attacker->attack_damage;

        self::reloadArmy($attacker);
        return $logData;
    }

    /**
     * @param $attacker
     */
    private static function reloadArmy($attacker): void
    {
        usleep($attacker->reload_time * 1000);
    }

}