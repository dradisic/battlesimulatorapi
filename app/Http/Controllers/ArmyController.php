<?php

namespace App\Http\Controllers;

use App\Army;
use App\Http\Factories\ArmyFactory;
use App\Http\Requests\ArmyStoreRequest;
use App\Http\Resources\ArmyResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ArmyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $armies = Army::query()->paginate();
        return ArmyResource::collection($armies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArmyStoreRequest $request
     * @return JsonResponse
     */
    public function store(ArmyStoreRequest $request): ?JsonResponse
    {
        $army = ArmyFactory::create($request->all());
        return response()->json([
            'message' => __('messages.army.created'),
            'data' => $army
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Army $army
     * @return JsonResponse
     */
    public function show(Army $army): JsonResponse
    {
        return response()->json([
            'data' => new ArmyResource($army)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Army $army
     * @return JsonResponse
     */
    public function update(Request $request, Army $army): ?JsonResponse
    {
        ArmyFactory::update($army, $request->all());
        return response()->json([
            'message' => __('messages.army.updated')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Army $army
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Army $army): ?JsonResponse
    {
        ArmyFactory::destroy($army);
        return response()->json([
            'message' => __('messages.army.deleted')
        ], 204);
    }
}
