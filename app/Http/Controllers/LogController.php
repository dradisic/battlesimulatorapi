<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Resources\LogCollection;
use App\Http\Resources\LogResource;
use App\Log;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return LogCollection
     */
    public function index(): LogCollection
    {
        return new LogCollection(Log::query()->paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param Game $game
     * @return AnonymousResourceCollection
     */
    public function show(Game $game)
    {
        return LogResource::collection($game->logs()->paginate());
    }
}
