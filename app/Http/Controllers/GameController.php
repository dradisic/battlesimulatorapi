<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Factories\GameFactory;
use App\Http\Resources\GameResource;
use App\Http\Services\GameHandler;
use App\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use RuntimeException;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return GameResource::collection(Game::query()->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return GameResource
     */
    public function store(): GameResource
    {
        $game = GameFactory::create();
        return new GameResource($game);

    }

    /**
     * @param Game $game
     * @return array
     */
    public function start(Game $game)
    {
        if ($game->status === Game::STATUS_FINISHED) {
            throw new RuntimeException('The game is already finished.');
        }

        if ($game->playerOne->armies->count() < 5) {
            throw new RuntimeException('Player one has not enough armies to start battle.');
        }

        if ($game->playerTwo->armies->count() < 5) {
            throw new RuntimeException('Player two has not enough armies to start battle.');
        }

        if ($game->overall_remaining_units > 1) {
            $game->status = Game::STATUS_IN_PROGRESS;

            if(GameHandler::startFight($game)) {
                $game->save();
            }
        }

        if ($game->playerOne->armies()->count() >= 1 && $game->playerTwo->armies()->count() === 0) {
            $game->winner = $game->playerOne;
            $game->status = Game::STATUS_FINISHED;
            $game->save();
        }

        if ($game->playerOne->armies()->count() === 0 && $game->playerTwo->armies()->count() >= 1) {
            $game->winner = $game->playerOne;
            $game->status = Game::STATUS_FINISHED;
            $game->save();
        }

        return [
            'game' => new GameResource($game),
            'logs' => Log::where('game_id', $game->id)->get(),
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param Game $game
     * @return GameResource
     */
    public function show(Game $game): GameResource
    {
        return new GameResource($game);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Game $game
     * @return JsonResponse
     */
    public function join(Game $game): JsonResponse
    {
        GameHandler::setUser(auth()->user(), $game);
        return response()->json([
           'message' => __('messages.users.set')
        ]);
    }
}
