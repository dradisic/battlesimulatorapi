<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\InvalidClientException;
use App\Exceptions\InvalidCredentialsException;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

class AuthController extends Controller
{
    public $successStatus = 200;

    public function register(RegisterRequest $request)
    {
        $input = $request->all();
        $user = new User;
        $user->fill($input);
        $user->email = $input['email'];
        $user->name = $input['name'];
        $user->password = Hash::make($input['password']);
        $user->save();

        $success['token'] = $user->createToken(config('app.name'))->accessToken;
        return response()->json(['success' => $success], $this->successStatus);
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $data = [
            'username' => $username,
            'password' => $password,
            'grant_type' => 'password',
            'client_id' => config('auth.client_id'),
            'client_secret' => config('auth.client_secret'),
            'scope' => '*'
        ];

        $newRequest =  app('request')->create('oauth/token', 'POST', $data );
        $res = app('router')->prepareResponse($newRequest, app()->handle($newRequest));
        $content = json_decode($res, true);

        if(isset($content['error']) && $content['error']  == 'invalid_credentials') {
            throw new InvalidCredentialsException();
        }
        if(isset($content['error']) && $content['error']  == 'invalid_client') {
            throw new InvalidClientException();
        }

        return $res;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    /**
     * Login user and create token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse [string] accessToken
     */
    public function refreshToken(Request $request)
    {
        $request->request->add([
            'refresh_token' => $request->refreshToken,
            'grant_type' => 'refresh_token',
            'client_id' => config('auth.client_id'),
            'client_secret' => config('auth.client_secret'),
            'scope' => '*'
        ]);

        $newRequest = Request::create('/oauth/token', 'post');

        return Route::dispatch($newRequest)->getContent();
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return JsonResponse [string] message
     */
    public function logout(Request $request)
    {
        $user = $request->user('api');

        if ($user) {
            $user->tokens->each(static function ($token) {
                $token->delete();
            });
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        }

        return response()->json([
            'message' => 'You are already logged out'
        ]);
    }
}