<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LogCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => $data->id,
                    'game' => new GameResource($data->game),
                    'attacker' => $data->attacker,
                    'target' => $data->target,
                    'strategy' => $data->strategy,
                    'damage' => $data->damage,
                ];
            }),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
