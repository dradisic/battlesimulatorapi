<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArmyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'starting_units' => $this->starting_units,
            'remaining_units' => $this->remaining_units,
            'is_defeated' => $this->is_defeated,
            'strategy' => new StrategyResource($this->strategy),
        ];
    }
}
