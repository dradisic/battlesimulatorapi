<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'player_one' => new UserResource($this->playerOne),
            'player_two' => new UserResource($this->playerTwo),
            'opponent' => new UserResource($this->opponent),
            'winner' => new UserResource($this->winner),
            'joined' => $this->alreadyJoined,
        ];
    }
}
