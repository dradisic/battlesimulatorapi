<?php

namespace App\Http\Resources;

use App\Strategy;
use Illuminate\Http\Resources\Json\JsonResource;

class LogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'attacker' => $this->attacker,
            'target' => $this->target,
            'strategy' => $this->strategy,
            'damage' => $this->damage,
        ];
    }
}
