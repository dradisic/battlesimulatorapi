<?php

namespace App\Http\Requests;

use App\Exceptions\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class BaseRequest extends FormRequest
{
    /**
     * @param Validator $validator
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, $this->buildResponse($validator));
    }

    /**
     * Throw new Failed Authorization Exception
     */
    protected function failedAuthorization()
    {
        throw new HttpException(401);
    }

    /**
     * @param Validator $validator
     * @return JsonResponse
     */
    protected function buildResponse(Validator $validator): JsonResponse
    {
        return response()->json([
            'message'=>$validator->errors(),
        ]);
    }
}
