<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UnconfirmedUserException extends UnauthorizedHttpException
{
    public function __construct($message = ErrorCodes::MESSAGES[ErrorCodes::UNCONFIRMED_USER], \Exception $previous = null, $code = ErrorCodes::UNCONFIRMED_USER)
    {
        parent::__construct('', $message, $previous, $code);
    }
}