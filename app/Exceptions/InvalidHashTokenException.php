<?php

namespace App\Exceptions;

use App\Exceptions\ErrorCodes;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class InvalidHashTokenException extends UnauthorizedHttpException
{
    public function __construct($message = ErrorCodes::MESSAGES[ErrorCodes::INVALID_HASH_TOKEN], \Exception $previous = null, $code = ErrorCodes::INVALID_TOKEN)
    {
        parent::__construct('', $message, $previous, $code);
    }
}