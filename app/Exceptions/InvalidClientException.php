<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class InvalidClientException extends UnauthorizedHttpException
{
    public function __construct($message = ErrorCodes::MESSAGES[ErrorCodes::INVALID_CLIENT], \Exception $previous = null, $code = ErrorCodes::INVALID_CREDENTIALS)
    {
        parent::__construct('', $message, $previous, $code);
    }
}