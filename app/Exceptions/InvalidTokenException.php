<?php

namespace App\Exceptions;

use App\Exceptions\ErrorCodes;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class InvalidTokenException extends UnauthorizedHttpException
{
    public function __construct($message = ErrorCodes::MESSAGES[ErrorCodes::INVALID_TOKEN], \Exception $previous = null, $code = ErrorCodes::INVALID_TOKEN)
    {
        parent::__construct('', $message, $previous, $code);
    }
}