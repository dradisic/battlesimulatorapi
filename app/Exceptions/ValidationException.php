<?php


namespace App\Exceptions;

use Illuminate\Validation\ValidationException as IlluminateValidationException;

class ValidationException extends IlluminateValidationException
{
    public function getStatusCode(): int
    {
        return $this->status;
    }
}