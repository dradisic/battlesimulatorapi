<?php

namespace App\Exceptions;

use Throwable;

/**
 * Class InvalidActionException
 * @package exceptions
 * @resource exceptions
 */
class InvalidActionException extends \Exception
{

    /**
     * @var int
     */
    protected $internalCode;

    /**
     * @var array
     */
    protected $details = [];

    /**
     * InvalidActionException constructor.
     * @param int $internalCode
     * @param array $details
     * @param int $statusCode
     * @param Throwable|null $previous
     */
    public function __construct($internalCode = 1000, $details = [], $statusCode = 406, Throwable $previous = null)
    {
        $message = "Invalid action";
        $this->internalCode = $internalCode;
        $this->details = $details;
        parent::__construct($message, $statusCode, $previous);
    }

    /**
     * @return int
     */
    public function getInternalCode()
    {
        return $this->internalCode;
    }

    /**
     * @return array
     */
    public function getDetails()
    {
        return $this->details;
    }

}