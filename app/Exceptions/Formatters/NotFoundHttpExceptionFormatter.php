<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class NotFoundHttpExceptionFormatter extends ExceptionFormatter
{

    public function format($exception)
    {
        $response = parent::format($exception);
        $this->setDetails(ErrorCodes::NOT_FOUND);
        return $response;
    }
}