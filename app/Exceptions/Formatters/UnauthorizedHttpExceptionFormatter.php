<?php
namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;
use Exception;

class UnauthorizedHttpExceptionFormatter extends ExceptionFormatter
{
    public function format($exception)
    {
        $response = parent::format($exception);
        $this->setDetails(ErrorCodes::UNAUTHORIZED);
        return $response;
    }
}