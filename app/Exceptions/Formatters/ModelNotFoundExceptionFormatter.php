<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class ModelNotFoundExceptionFormatter extends ExceptionFormatter
{

    public function format($exception)
    {
        $response = parent::format($exception);
        $this->setDetails(ErrorCodes::NOT_FOUND);
        return $response;
    }
}