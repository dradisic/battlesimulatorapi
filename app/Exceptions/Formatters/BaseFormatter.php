<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;
use Illuminate\Http\JsonResponse;

abstract class BaseFormatter
{
    /**
     * @var $config
     */
    protected $config;

    /**
     * @var $debug
     */
    protected $debug;

    /**
     * @var JsonResponse
     */
    protected $response;

    /**
     * BaseFormatter constructor.
     * @param array $config
     * @param $debug
     */
    public function __construct(array $config, $debug)
    {
        $this->debug = $debug;
        $this->config = $config;
    }

    /**
     * @param $exception
     * @return JsonResponse
     */
    abstract protected function format($exception);

    /**
     * @return JsonResponse
     */
    protected function getResponse() {
        if($this->response instanceof JsonResponse) {
            return $this->response;
        }
        return $this->response = new JsonResponse();
    }

    /**
     * Set code and message
     * @param $code
     * @param array $details
     * @return bool
     * @throws \Exception
     */
    protected function setDetails($code, $details = []) {

        if(!isset(ErrorCodes::MESSAGES[$code])) {
            throw new \Exception('Undefined Error code');
        }

        $responseData = json_decode(json_encode($this->getResponse()->getData()), true);

        if(!isset($responseData['error'])) {
            $responseData['error'] = [];
        }

        $responseData['error'] = array_merge($responseData['error'], [
            'code' => $code,
            'message' => ErrorCodes::MESSAGES[$code],
            'details' => $details,
        ]);

        $this->getResponse()->setData($responseData);

        return true;
    }
}