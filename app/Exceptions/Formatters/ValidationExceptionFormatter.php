<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class ValidationExceptionFormatter extends ExceptionFormatter
{

    public function format($exception)
    {
        $response = parent::format($exception);
        $this->setDetails(ErrorCodes::VALIDATION_ERROR, $exception->errors());
        return $response;
    }
}