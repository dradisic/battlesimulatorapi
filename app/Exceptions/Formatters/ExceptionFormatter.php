<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class ExceptionFormatter
 * @package App\Exceptions\Formatters
 *
 */
class ExceptionFormatter extends BaseFormatter
{

    /**
     * @param $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function format($exception)
    {
        $this->getResponse()->setStatusCode(500);

        $data = [
            'code' => empty($exception->getCode()) ? ErrorCodes::UNDEFINED : $exception->getCode(),
            'message' => ErrorCodes::MESSAGES[ErrorCodes::UNDEFINED],
            'details' => [],
        ];

        if ($this->debug) {
            $data = array_merge($data, [
                'message' => empty($exception->getMessage()) ? ErrorCodes::MESSAGES[ErrorCodes::UNDEFINED] : $exception->getMessage(),
                'exception' => (string)$exception,
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ]);
        }

        if ($exception instanceof HttpExceptionInterface) {
            $this->getResponse()->setStatusCode($exception->getStatusCode());
            if (count($headers = $exception->getHeaders())) {
                $this->getResponse()->headers->add($headers);
            }
        }

        $this->getResponse()->setData([
            'error' => $data
        ]);

        return $this->getResponse();
    }
}