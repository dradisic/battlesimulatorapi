<?php

namespace App\Exceptions\Formatters;

class HttpExceptionFormatter extends ExceptionFormatter
{

    public function format($exception)
    {
        $response = parent::format($exception);
        return $response;
    }
}