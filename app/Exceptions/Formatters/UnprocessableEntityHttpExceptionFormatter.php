<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class UnprocessableEntityHttpExceptionFormatter extends ExceptionFormatter
{
    public function format($exception)
    {
        $response = parent::format($exception);
        $response->setStatusCode(422);

        // Laravel validation errors will return JSON string
        $decoded = json_decode($exception->getMessage(), true);

        // Message was not valid JSON
        // This occurs when we throw UnprocessableEntityHttpExceptions
        if (json_last_error() !== JSON_ERROR_NONE) {
            // Mimick the structure of Laravel validation errors
            $decoded = [[$exception->getMessage()]];
        }

        $this->setDetails(ErrorCodes::VALIDATION_ERROR, $decoded);

        return $response;
    }
}