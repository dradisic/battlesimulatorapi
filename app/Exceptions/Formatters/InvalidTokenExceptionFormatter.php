<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class InvalidTokenExceptionFormatter extends ExceptionFormatter
{

    public function format($exception)
    {
        $response = parent::format($exception);
        $this->setDetails(ErrorCodes::INVALID_TOKEN);
        return $response;
    }
}