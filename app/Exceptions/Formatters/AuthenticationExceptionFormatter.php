<?php
namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class AuthenticationExceptionFormatter extends ExceptionFormatter
{
    public function format($exception)
    {
        $response = parent::format($exception);
        $response->setStatusCode(401);
        $this->setDetails(ErrorCodes::UNAUTHENTICATED);
        return $response;
    }
}