<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class InvalidHashTokenExceptionFormatter extends ExceptionFormatter
{

    public function format($exception)
    {
        $response = parent::format($exception);
        $this->setDetails(ErrorCodes::INVALID_HASH_TOKEN);
        return $response;
    }
}