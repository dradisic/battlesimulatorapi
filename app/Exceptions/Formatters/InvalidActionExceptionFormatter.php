<?php

namespace App\Exceptions\Formatters;

use App\Exceptions\ErrorCodes;

class InvalidActionExceptionFormatter extends ExceptionFormatter
{
    public function format($exception)
    {
        $response = parent::format($exception);
        $response->setStatusCode($exception->getCode());

        $data = [
            'code' => $exception->getInternalCode(),
            'message' => ErrorCodes::MESSAGES[$exception->getInternalCode()] ?? ErrorCodes::MESSAGES[ErrorCodes::UNDEFINED],
            'details' => $exception->getDetails(),
        ];

        $response->setData([
            'error' => $data,
        ]);

        return $response;
    }
}