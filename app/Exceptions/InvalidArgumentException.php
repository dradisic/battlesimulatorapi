<?php

namespace App\Exceptions;

use App\Exceptions\ErrorCodes;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class InvalidArgumentException extends UnauthorizedHttpException
{
    public function __construct($message = ErrorCodes::MESSAGES[ErrorCodes::INVALID_ARGUMENT], \Exception $previous = null, $code = ErrorCodes::INVALID_ARGUMENT)
    {
        parent::__construct('', $message, $previous, $code);
    }
}