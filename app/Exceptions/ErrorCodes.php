<?php

namespace App\Exceptions;

class ErrorCodes
{
    const UNDEFINED = 1000;
    const NOT_FOUND = 1001;
    const UNAUTHENTICATED = 1002;
    const UNAUTHORIZED = 1003;
    const INVALID_CREDENTIALS = 1004;
    const UNVERIFIED_ACCOUNT = 1005;
    const VALIDATION_ERROR = 1006;
    const INVALID_TOKEN = 1007;
    const TOKEN_NOT_REFRESHABLE = 1008;
    const SOCIAL_SIGN_UP_FIRST = 1009;
    const INVALID_CLIENT = 1010;
    const INVALID_ENCRYPTION_KEY = 1011;
    const INVALID_HASH_TOKEN = 1012;
    const INVALID_ARGUMENT = 1015;
    const UNCONFIRMED_USER = 1016;
    const INVALID_QUERY_PARAMETER = 1017;
    const INVALID_IP_ADDRESS = 1018;
    const CACHE_SESSION = 1019;
    const MISSING_CATALOG_ID = 1020;

    const MESSAGES = [
        self::UNCONFIRMED_USER => 'Ваш налог још није потврђен од стране администратора.',
        self::UNDEFINED => 'Undefined',
        self::NOT_FOUND => 'Not found',
        self::UNAUTHENTICATED => 'Unauthenticated',
        self::UNAUTHORIZED => 'You are not authorized to access this resource!',
        self::INVALID_CREDENTIALS => 'Invalid credentials',
        self::INVALID_CLIENT => 'Invalid client',
        self::UNVERIFIED_ACCOUNT => 'Unverified account',
        self::VALIDATION_ERROR => 'Validation error',
        self::INVALID_TOKEN => 'Invalid token',
        self::INVALID_HASH_TOKEN => 'Invalid hash token',
        self::TOKEN_NOT_REFRESHABLE => 'Token not refreshable',
        self::SOCIAL_SIGN_UP_FIRST => 'Social sign up first',
        self::INVALID_ENCRYPTION_KEY => 'Invalid encryption key',
        self::INVALID_ARGUMENT => 'Invalid argument',
        self::INVALID_QUERY_PARAMETER => 'Query parameters are required!',
        self::INVALID_IP_ADDRESS => 'Access denied',
        self::CACHE_SESSION => 'Unable to cache session id.',
        self::MISSING_CATALOG_ID => 'Missing Catalog id.',
    ];

}