<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $guard = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'random_army',
        'weakest_army',
        'strongest_army',
    ];

    /**
     * @return HasMany
     */
    public function armies(): HasMany
    {
        return $this->hasMany(Army::class);
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder|object|null
     */
    public function getRandomArmyAttribute()
    {
        return $this->armies()->inRandomOrder()->first();
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder|object|null
     */
    public function getWeakestArmyAttribute()
    {
        return $this->armies()->orderBy('starting_units', 'ASC')->first();
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder|object|null
     */
    public function getStrongestArmyAttribute()
    {
        return $this->armies()->orderBy('starting_units', 'DESC')->first();
    }
}
