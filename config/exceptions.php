<?php

use App\Exceptions\Formatters as Formatters;
use Symfony\Component\HttpKernel\Exception as SymfonyException;

return [
    'formatters' => [
        \App\Exceptions\InvalidActionException::class => Formatters\InvalidActionExceptionFormatter::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class => Formatters\ModelNotFoundExceptionFormatter::class,
        SymfonyException\UnprocessableEntityHttpException::class => Formatters\UnprocessableEntityHttpExceptionFormatter::class,
        SymfonyException\NotFoundHttpException::class => Formatters\NotFoundHttpExceptionFormatter::class,
        Illuminate\Auth\AuthenticationException::class => Formatters\AuthenticationExceptionFormatter::class,
        SymfonyException\UnauthorizedHttpException::class => Formatters\UnauthorizedHttpExceptionFormatter::class,
        SymfonyException\HttpException::class => Formatters\HttpExceptionFormatter::class,
        Illuminate\Validation\ValidationException::class => Formatters\ValidationExceptionFormatter::class,
        Exception::class => Formatters\ExceptionFormatter::class,
        App\Exceptions\InvalidTokenException::class => Formatters\InvalidTokenExceptionFormatter::class,
    ],
    'server_error_production' => 'An error has occurred.'
];