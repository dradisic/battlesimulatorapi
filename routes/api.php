<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1'
], function () {
    Route::group([
        'namespace' => 'Auth'
    ], function () {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('register', 'AuthController@register');
        Route::post('login/refresh', 'AuthController@refreshToken')->name('login.refresh');
    });

    Route::group([
        'middleware' => [
            'auth:api'
        ]
    ], function () {
        Route::resource('armies', 'ArmyController', [
            'except' => ['edit', 'create']
        ]);
        Route::group([
            'prefix' => 'games'
        ], function () {
                Route::get('', 'GameController@index');
                Route::post('', 'GameController@store');
                Route::get('{game}/join', 'GameController@join');
                Route::get('{game}/start', 'GameController@start');
            });
        Route::group([
            'prefix' => 'logs'
        ], function () {
                Route::get('', 'LogController@index');
                Route::get('/{game}', 'LogController@show');
            });
        Route::group([
            'prefix' => 'strategies'
        ], function () {
                Route::get('', 'StrategyController@index');
            });
    });
});
