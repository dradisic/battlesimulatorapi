<?php

return [
    'army' => [
        'created' => 'Army has been successfully created.',
        'deleted' => 'Army has been successfully deleted.',
        'updated' => 'Army has been successfully updated.'
    ],
    'users' => [
        'set' => 'You are set up for the game.'
    ]
];