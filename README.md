# BattleSimulator api

Api for BattleSimulator client application

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Installing

- ```composer install```
- ```cp .env.example .env```

Generate your encryption key
- ```php artisan key:generate```

Run database migration and seeding
- ```php artisan migrate --seed```

Generate client secret
- ```php artisan passport:install```

Copy client id and client secret into your .env
- ```
    CLIENT_ID=YOUR-PASSPORT-CLIENT-ID
    CLIENT_SECRET=YOUR-PASSPORT-CLIENT-SECRET
  ```

Update env file
- ```
     APP_URL=https://YOUR-API-DOMAIN-NAME
     CLIENT_APP_URL=https://YOUR-CLIENT-DOMAIN-NAME:8081
  ```

## Built With

* [Laravel](https://laravel.com/) - Php framework used

## Authors

* **Dragan Radisic**
